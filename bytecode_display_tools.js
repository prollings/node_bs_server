"use strict"

function to16BitArray (array, offset, range) {
  let high = offset + (range / 2.0);
  let low  = offset - (range / 2.0);
  let slope = 65535 / (high - low);
  let retArray = new Uint16Array(array.map(function (num) {
    return slope * (num - low);
  }));
  return retArray;
}

function waveToPointArray (inputArray, offset, range, xFrame, yFrame) {
  const high = offset + (range / 2);
  const low = offset - (range / 2);
  const slope = (yFrame[0] - yFrame[1]) / (high - low);
  const xIncrement = (xFrame[1] - xFrame[0]) / inputArray.length;
  let xPos = xFrame[0];
  let x, y;
  let retArray = new Uint32Array(inputArray.map(function (num) {
    x = (xPos << 16) >>> 0;
    y = Math.round(yFrame[1] + slope * (num - low));
    xPos += xIncrement;
    return x + y;
  }));
  return retArray;
}

let executionVectors = {
  polyLine : "0",
  polyFill : "1",
  clear    : "2",
  text     : "3",
};

function pointsToPoly (inputArray, width, colour) {
  let bcStr = "";
  let num;
  for (num in inputArray) {
    bcStr += "[" + inputArray[num].toString(16) + "]";
  }
  bcStr += "[" + width.toString(16) + "]";
  bcStr += "[" + colour + "]";
  bcStr += "[" + inputArray.length.toString(16) + "]";
  return bcStr;
}

function polyLine (inputArray, width, colour) {
  let bcStr = pointsToPoly(inputArray, width, colour);
  bcStr += "[" + executionVectors.polyLine + "]X";
  return bcStr;
}

function polyFill (inputArray, width, colour) {
  let bcStr = pointsToPoly(inputArray, width, colour);
  bcStr += "[" + executionVectors.polyFill + "]X";
  return bcStr;
}

function clear (colour) {
  return "[" + colour + "][2]X";
}

function text (text, xPos, yPos, size, colour) {
  let bcStr;
  let pos = ((xPos << 16 >>> 0) + yPos).toString(16);
  let iii, character;
  for (iii in text) {
    bcStr += "[" + text[iii].charCodeAt(0).toString(16) + "]";
  }
  bcStr += "[" + pos + "][" + size.toString(16) + "]";
  bcStr += "[" + colour + "][" + text.length.toString(16) + "]";
  bcStr += "[" + executionVectors.text + "]X";
  return bcStr;
}

exports.to16BitArray = to16BitArray;
exports.waveToPointArray = waveToPointArray;
exports.polyLine = polyLine;
exports.polyFill = polyFill;
exports.clear = clear;
exports.stringToText = text;
