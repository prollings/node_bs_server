"use strict"

let executionVectors = {
  cursor : "00"
};

function makeCursor (id, xRange, yRange, colour, width, direction) {
  let topLeft = ((xRange[0] << 16 >>> 0) + yRange[0]).toString(16);
  let bottomRight = ((xRange[1] << 16 >>> 0) + yRange[1]).toString(16);
  let bcStr = "";
  bcStr += "[" + bottomRight + "][" + topLeft + "]";
  bcStr += "[" + width.toString(16) + "]";
  bcStr += "[" + direction.toString(16) + "]";
  bcStr += "[" + colour + "]";
  bcStr += "[" + selector.toString(16) + "]";
  bcStr += "[" + executionVectors.cursor + "]";
  return bcStr;
}

exports.makeCursor = makeCursor;
