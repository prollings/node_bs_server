"use strict"

let bitscope = new require("bitscope-js").Bitscope();
let bcDisplay = require("./bytecode_display_tools.js");
let bcInteraction = require("./bytecode_interaction_tools.js");
let numTools = require("./number_tools.js");
let server = require("./websocket_manager.js");
let pageServer = require("./page_server.js");

pageServer.start();

server.start();

let offset = 0;
let ranges = [1.1, 3.5, 5.2, 9.2];
let range = 1;
let size = 500;

let capturing = true;

/* CALLBACK QUEUE */

let callbackQueue = [];

function pushToCallbackQueue(callback) {
  callbackQueue.push(callback);
}

function runCallbackQueue() {
  let idx;
  for (idx in callbackQueue) {
    callbackQueue[idx]();
  }
  clearCallbackQueue();
}

function clearCallbackQueue() {
  callbackQueue = [];
}

/* DOC STACK STUFF */

let docStack = [];
let docStackPtr = 1023;
let docStackSize = 1024;

let valReg, addrReg;

function pushToDocStack (val) {
  docStackPtr = docStackPtr == 0 ? docStackSize - 1 : docStackPtr - 1;
  docStack[docStackPtr] = val;
}

let docElements = {};

// Trigger toggle
docElements[0] = {
  type    : "button",
  onClick : function () {
    bitscope.setOptions({"trigger_enable" : !bitscope.getOptions()["trigger_enable"] });
  }
};

// Layout toggle
docElements[1] = {
  type    : "button",
  onClick : function () {
    yFrameLayout = yFrameLayout == "overlay" ? "separate" : "overlay";
  }
};

// Rate change / timebase
docElements[2] = {
  type    : "slider",
  onInput : function (value) {
    let normValue = value / 65535;
    let rateVal = 30000 + (1000000 * normValue);
    bitscope.setOptions({
      "rate" : Math.round(rateVal),
      "size" : size
    });
  }
};

// Reset options
docElements[3] = {
  type    : "button",
  onClick : function () {
    resetBitscope();
  }
};

// Trigger level
docElements[4] = {
  type    : "slider",
  onInput : function (value) {
    let normValue = value / 65535;
    let triggerVal = (normValue - 0.5) * 10.0;
    bitscope.setOptions({"trigger_level" : triggerVal});
  }
};

// Range up
docElements[5] = {
  type    : "button",
  onClick : function () {
    let currentRange = bitscope.getOptions()["range"];
    range = currentRange + 1;
    if (range > 3)
      range = 3;
    bitscope.setOptions({"range" : range});
  }
};

// Range down
docElements[6] = {
  type    : "button",
  onClick : function () {
    let currentRange = bitscope.getOptions()["range"];
    range = currentRange - 1;
    if (range < 0)
      range = 0;
    bitscope.setOptions({"range" : range});
  }
};

let docDirtyElements = []; // Ids only!

let docStatusFunctions = {};

docStatusFunctions[0] = function () {
  // Received: List of dirty elements
  // Format:
  //   ptr+1 = count
  //   ptr+n = element id
  let elementCount = docStack[docStackPtr+1];
  if (elementCount == 0)
    return;
  let iii;
  for (iii = 0; iii < elementCount; iii++) {
    docDirtyElements.push(docStack[docStackPtr + 2 + iii]);
  }
};

docStatusFunctions[1] = function () {
  // Received: Specific element's status
  // Format:
  //   ptr+1 = element id
  //   ptr+n = element type specific values
  let elementId = docStack[docStackPtr + 1];
  let element = docElements[elementId];
  if (element.type == "button") {
    let numberOfClicks = docStack[docStackPtr + 2];
    // Execute button's callback that number of times
    let iii;
    for (iii = 0; iii < numberOfClicks; iii++) {
      pushToCallbackQueue(element.onClick);
    }
  }
  else if (element.type == "slider") {
    let value = docStack[docStackPtr + 2];
    pushToCallbackQueue(function () { element.onInput(value); });
  }
};

function executeStatusFunction () {
  let fnId = docStack[docStackPtr];
  docStatusFunctions[fnId]();
}

function docDecode (bc, offset) {
  let char, iii;
  let bcLength = bc.length;
  for (iii = offset; iii < bcLength; iii++) {
    char = bc[iii];
    switch (char) {
      case "S":
        executeStatusFunction();
        docStackPtr = docStackSize - 1;
        break;
      case "[":
        valReg = "";
        break;
      case "]":
        pushToDocStack(parseInt(valReg, 16));
        break;
      default:
        valReg += char;
        break;
    }
  }
}

server.setRecieveCallback(function (str) {
  if (str[0] == "2") {
    // Document!
    docDecode(str, 1);
  }
});

/* BITSCOPE */
function resetBitscope() {
  bitscope.setOptions({
    "device" : 0,
    "mode"   : 1,
    "size"   : size,
    "rate"   : 100000,
    "range"  : range,
    "offset" : -offset,

    "channels" : [0, 1],

    "trace_timeout"   : 0.05,
    "trigger_channel" : 0,
    "trigger_token"   : 0,
    "trigger_level"   : 0.8,
    "trigger_enable"  : true,

    "intro": 0,
    "delay": 0
  });
}

resetBitscope();

let xFrame = [0,65535];
let yFrameLayout = "overlay";
let yFrame = [10000, 55000];
let yLayouts = {
  "overlay"  : { "a" : yFrame, "b" : yFrame},
  "separate" : { "a" : [6553,26214], "b" : [39321, 58981] }
};

function capture() {
  if (!capturing)
    return; // Early return
  runCallbackQueue();
  bitscope.capture(function () {
    let buffers = bitscope.getBuffers();
    let xFrame = [0,65535];
    // xFrame = [65535, 0]; // Hack fix for trace direction! D:
    let yFrameA = yLayouts[yFrameLayout]["a"];
    let yFrameB = yLayouts[yFrameLayout]["b"];
    let normBuffers = {
      0 : bcDisplay.waveToPointArray(buffers[0], offset, ranges[range], xFrame, yFrameA),
      1 : bcDisplay.waveToPointArray(buffers[1], offset, ranges[range], xFrame, yFrameB)
    };
    let strBuffers = {
      0 : bcDisplay.polyLine(normBuffers[0], 2, "dddd00"),
      1 : bcDisplay.polyLine(normBuffers[1], 2, "00dd00")
    };
    let idx = 0;
    let outStr = bcDisplay.clear("404040");
    for (idx in strBuffers) {
      outStr += strBuffers[idx];
    }
    server.sendText("0" + outStr);
    setTimeout(capture, 50);
  });
}

function getDocumentStatus() {
  let eleStatusStr = "2";
  let idx, elementId;
  for (idx in docDirtyElements) {
    elementId = docDirtyElements[idx];
    eleStatusStr += "[" + elementId + "][1]S";
  }
  docDirtyElements = [];
  server.sendText(eleStatusStr + "[0]S");
  setTimeout(getDocumentStatus, 50);
}

capture();
getDocumentStatus();
