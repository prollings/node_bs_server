"use strict"

exports.normaliseArray = function (array, offset, range) {
  let high = offset + (range / 2.0);
  let low  = offset - (range / 2.0);
  let slope = 1.0 / (high - low);
  let retArray = new Float32Array(array.map(function (num) {
    return slope * (num - low);
  }));
  return retArray;
};

exports.toRange = function (value, offset, range) {
  let halfRange = range / 2;
  let high = offset + halfRange;
  let low = offset - halfRange;
  let slope = (high - low) / 1.0;
  return high + slope * (value - 1.0);
};

exports.getMinMax = function (array) {
  let min = 1.0;
  let max = 0.0;
  array.forEach(function (num) {
    if (num > max)
      max = num;
    if (num < min)
      min = num;
  });
  return {"min" : min, "max" : max };
};
