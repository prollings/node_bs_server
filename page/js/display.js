var bcDisplay = {};

bcDisplay.canvas;
bcDisplay.ctx;

bcDisplay.rValMap = {};

for (var iii = 0; iii < 100; iii++) {
  bcDisplay.rValMap[iii] = 0;
}
bcDisplay.rValMap[0x00] = "";

bcDisplay.stack = [];
bcDisplay.stackPtr = 0;
bcDisplay.stackSize = 1024;

bcDisplay.setCanvas = function (canvas) {
  this.canvas = canvas;
  this.ctx = canvas.getContext("2d");
}

bcDisplay.pushToStack = function(val) {
  // Move the ptr back
  this.stackPtr = this.stackPtr == 0 ? this.stackSize-1 : this.stackPtr-1;
  // Put val onto stack
  this.stack[this.stackPtr] = val;
}

bcDisplay.stackFunctions = {};

bcDisplay.rNameMap = {
  valReg         : 0x00,
  addrReg        : 0x01,
  xPos           : 0x02,
  yPos           : 0x03,
  xSize          : 0x04,
  ySize          : 0x05,
  colour         : 0x06,
  shape          : 0x07,  /* primative */
  drawMode       : 0x08,  /* fill or outline */
  lineWidth      : 0x09,
  stackFunction  : 0x0a,
  stackLength    : 0x0b,
  polyClose      : 0x0c
};

bcDisplay.decode = function (bc, offset) {
  var char, iii;
  var rvm = this.rValMap;
  var rnm = this.rNameMap;
  var bcLength = bc.length;
  for (iii = offset; iii < bcLength; iii++) {
    char = bc[iii];
    switch (char) {
      case "X":
        // Executres stack function
        // then clears the stack
        this.executePureStackFunction();
        this.stackPtr = this.stackSize - 1;
        break;
      case ".":
        // Clears stack and resets ptr
        this.stack = [];
        this.stackPtr = this.stackSize - 1;
        break;
      case "[":
        // Clear reg 00
        rvm[rnm.valReg] = "";
        break;
      case "]":
        // Push stuff onto the stack
        // stack.push(parseInt(rValMap[rNameMap.valReg], 16));
        this.pushToStack(parseInt(rvm[rnm.valReg], 16));
        break;
      case "@":
        // Puts reg 00 into reg 01
        rvm[rnm.addrReg] = parseInt(rvm[rnm.valReg], 16);
        break;
      case "s":
        // Puts reg 00 into the reg pointed at by reg 01
        rvm[rvm[rnm.addrReg]] = parseInt(rvm[rnm.valReg], 16);
        break;
      case "n":
        rvm[rvm[rnm.addrReg]] += 1;
        break;
      default:
        rvm[rnm.valReg] += char;
        break;
    }
  }
}

/* RENDER HELPERS */

bcDisplay.toColour = function(col) {
  return "#" + "0".repeat(6 - col.length) + col;
}

bcDisplay.getPixelValues = function(x, y) {
  var cWidth = this.canvas.width;
  var cHeight = this.canvas.height;
  return {
    x : x * (cWidth / 65536),
    y : y * (cHeight / 65536)
  };
}

bcDisplay.unpackXY = function(val) {
  var y = val & 0xffff;
  var x = (val - y) >>> 16;
  return [x, y];
}

bcDisplay.packedPolyPathHelper = function(ctx, close, start, length) {
  ctx.beginPath();
  var xy = this.unpackXY(this.stack[start]);
  var xyo = this.getPixelValues(xy[0], xy[1]);
  ctx.moveTo(xyo.x, xyo.y);
  var iii;
  for (iii = 1; iii < length; iii++) {
    xy = this.unpackXY(this.stack[iii + start]);
    xyo = this.getPixelValues(xy[0], xy[1]);
    ctx.lineTo(xyo.x, xyo.y);
  }
  if (close > 0)
    ctx.closePath();
}

bcDisplay.packedTextHelper = function (start, length) {
  var text = "";
  var iii;
  for (iii = 0; iii < length; iii++) {
    text += String.fromCharCode(this.stack[iii + start]);
  }
  return text;
};

/* PURE STACK FUNCTIONS */

bcDisplay.executePureStackFunction = function() {
  // The stackPtr is the index of the
  // selected execution vector
  var functionId = this.stack[this.stackPtr];
  this.stackFunctions[functionId](this);
}

// Poly line
bcDisplay.stackFunctions[0x00] = function (bcd) {
  /* stackPtr offsets:
   * points : 1
   * colour : 2
   * width  : 3
   */
  // Get data off the stack
  var sp = bcd.stackPtr;
  var points = bcd.stack[sp + 1];
  var colour = bcd.toColour(bcd.stack[sp + 2].toString(16));
  var width = bcd.stack[sp + 3];
  bcd.ctx.strokeStyle = colour;
  bcd.ctx.lineWidth = width;
  bcd.packedPolyPathHelper(bcd.ctx, false, sp + 4, points);
  bcd.ctx.stroke();
};

// Poly fill
bcDisplay.stackFunctions[0x01] = function (bcd) {
  /* stackPtr offsets:
   * points : 1
   * colour : 2
   */
  var sp = bcd.stackPtr;
  var points = bcd.stack[sp + 1];
  var colour = bcd.toColour(bcd.stack[sp + 2].toString(16));
  bcd.ctx.fillStyle = colour;
  bcd.packedPolyPathHelper(bcd.ctx, true, sp + 3, points);
  bcd.ctx.fill();
};

// Blank screen
bcDisplay.stackFunctions[0x02] = function (bcd) {
  /* stackPtr offsets:
   * colour : 1
   */
  var sp = bcd.stackPtr;
  var colour = bcd.toColour(bcd.stack[sp + 1].toString(16));
  bcd.ctx.fillStyle = colour;
  bcd.ctx.fillRect(0,0, bcd.canvas.width, bcd.canvas.height);
};

// Text
bcDisplay.stackFunctions[0x03] = function (bcd) {
  /* stackPtr offsets:
   * length : 1
   * colour : 2,
   * size : 3,
   * pos : 4
   */
   var sp = bcd.stackPtr;
   var chars = bcd.stack[sp + 1];
   var colour = bcd.toColour(bcd.stack[sp + 2].toString(16));
   var size = bcd.stack[sp + 3];
   var pos = bcd.unpackXY(bcd.stack[sp + 4]);
   var text = bcd.packedTextHelper(sp + 5, chars);

   pos = bcd.getPixelValues(pos[0], pos[1]);
   bcd.ctx.font = size.toString() + "px Arial";
   bcd.ctx.fillStyle = colour;
   bcd.ctx.fillText(text, pos.x, pos.y);
};
