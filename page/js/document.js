var bcDocument = {};

bcDocument.elements = {};


/* STACK */

bcDocument.stack = [];
bcDocument.stackPtr = 1023;
bcDocument.stackSize = 1024;

bcDocument.pushToStack = function (val) {
  this.stackPtr = this.stackPtr == 0 ? this.stackSize - 1 : this.stackPtr - 1;
  this.stack[this.stackPtr] = val;
};

bcDocument.stackFunctions = {};
bcDocument.statusFunctions = {};

bcDocument.executeStackFunction = function () {
  var functionId = this.stack[this.stackPtr];
  this.stackFunctions[functionId](this);
};

bcDocument.executeStatusFunction = function () {
  var fnId = this.stack[this.stackPtr];
  var status = this.statusFunctions[fnId](this);
  this.respond("2" + status);
};

/* REGISTERS */

bcDocument.rValMap = {
  0x00 : "",
  0x01 : 0
};

bcDocument.rNameMap = {
  valReg  : 0x00,
  addrReg : 0x01
};
/* RESPOND */

bcDocument.respond = function (msg) {
  // Must be overwritten in manager
  console.log("Default responder:", msg);
};

/* BYTECODE */

bcDocument.decode = function (bc, offset) {
  var char, iii;
  var rvm = this.rValMap;
  var rnm = this.rNameMap;
  var bcLength = bc.length;
  for (iii = offset; iii < bcLength; iii++) {
    char = bc[iii];
    switch (char) {
      case "X":
        this.executeStackFunction();
        this.stackPtr = this.stackSize - 1;
        break;
      case "S":
        this.executeStatusFunction();
        this.stackPtr = this.stackSize - 1;
        break;
      case "[":
        rvm[rnm.valReg] = "";
        break;
      case "]":
        this.pushToStack(parseInt(rvm[rnm.valReg], 16));
        break;
      default:
        rvm[rnm.valReg] += char;
        break;
    }
  }
};

/* ELEMENT REGISTER FUNCTIONS */

/* This works in reverse to how bcInteract does.
 * The elements are pre-existing on the page, and
 * have to be registered within this object.
 * It's then up to whoever implented the HTML page
 * to keep it in sync with what the server thinks
 * is going on.
 */

bcDocument.registerButton = function (element, selector) {
  var bcDoc = this;
  element.addEventListener("click", function (evt) {
    bcDoc.elements[selector].dirty = true;
  });
  this.elements[selector] = {
    type  : "button",
    dirty : false
  };
};

bcDocument.registerSlider = function (element, selector) {
  var bcDoc = this;
  element.addEventListener("input", function (evt) {
    bcDoc.elements[selector].dirty = true;
    bcDoc.elements[selector].value = element.value;
  });
  this.elements[selector] = {
    type : "slider",
    dirty : true, // True by default on purpose.
    value : element.value
  };
};

/* STATUS FUNCTIONS */

bcDocument.statusCodes = {
  button : 0
};

// Get all status
bcDocument.statusFunctions[0x00] = function (bcDoc) {
  var ele, idx;
  var count = 0;
  var dirtyElements = {};
  for (idx in bcDoc.elements) {
    ele = bcDoc.elements[idx];
    if (ele.dirty) {
      dirtyElements[idx] = ele;
      count++;
    }
  }
  var bcStr = "";
  for (idx in dirtyElements) {
    ele = dirtyElements[idx];
    bcStr += "[" + idx.toString(16) + "]";
  }
  bcStr += "[" + count.toString(16) + "][00]S";
  return bcStr;
};

// Get one status
bcDocument.statusFunctions[0x01] = function (bcDoc) {
  // Ex: [5][1]S
  // AKA: Get status([1]) of widget 5([5])
  // Returns: [1][5][1]S if it's been clicked.
  var elementId = bcDoc.stack[bcDoc.stackPtr + 1];
  var element = bcDoc.elements[elementId];
  var bcStr = "";
  if (element.dirty) {
    switch (element.type) {
      case "button":
        bcStr += buttonStatus(element);
        break;
      case "slider":
        bcStr += sliderStatus(element);
        break;
    }
    return bcStr + "[" + elementId.toString(16) + "][1]S";
  }
  else
    return "";
};

function buttonStatus(element) {
  // Returns the number of times it's
  // been clicked since the last reset.
  var clicks = "[" + (element.dirty + 0) + "]";
  element.dirty = false;
  return clicks;
}

function sliderStatus(element) {
  // Return the position of the slider
  // 0 -> 65535
  var value = "[" + Math.round(element.value * 655.35).toString(16) + "]";
  element.dirty = false;
  return value;
}
