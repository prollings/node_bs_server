var bcInteract = {};

bcInteract.canvas;
bcInteract.ctx;

/* ENTITIES */
bcInteract.entities = {};

/* STACK */

bcInteract.stack = [];
bcInteract.stackPtr = 1023;
bcInteract.stackSize = 1024;

bcInteract.setCanvas = function (canvas) {
  this.canvas = canvas;
  this.ctx = canvas.getContext("2d");
  this.connectCanvasEvents();
  var bci = this;
  setInterval(function () { bci.render(); }, 20);
}

bcInteract.pushToStack = function (val) {
  // Move the ptr back
  this.stackPtr = this.stackPtr == 0 ? this.stackSize-1 : this.stackPtr-1;
  // Put val onto stack
  this.stack[this.stackPtr] = val;
};

bcInteract.stackFunctions = {};
bcInteract.statusFunctions = {};
/* REGISTERS */

bcInteract.rValMap = {
  0x00 : "",
  0x01 : 0
};

bcInteract.rNameMap = {
  valReg  : 0x00,
  addrReg : 0x01
};

/* OPERATION */

bcInteract.respond = function (msg) {
  // Default response. Must be overwritten
  // to talk to server
  console.log(msg);
};

bcInteract.decode = function (bc, offset) {
  var char, iii;
  var rvm = this.rValMap;
  var rnm = this.rNameMap;
  var bcLength = bc.length;
  for (iii = offset; iii < bcLength; iii++) {
    char = bc[iii];
    switch (char) {
      case "X":
        this.executeStackFunction();
        this.stackPtr = this.stackSize - 1;
        break;
      case "S":
        this.executeStatusFunction();
        this.stackPtr = this.stackSize - 1;
        break;
      case ".":
        this.stack = [];
        this.stackPtr = this.stackSize - 1;
        break;
      case "[":
        // Clear reg 00
        rvm[rnm.valReg] = "";
        break;
      case "]":
        // Push r00 onto the stack
        this.pushToStack(parseInt(rvm[rnm.valReg], 16));
        break;
      default:
        rvm[rnm.valReg] += char;
        break;
    }
  }
};

/* STATUS */

bcInteract.executeStatusFunction = function () {
  var fnId = this.stack[this.stackPtr];
  var status = this.statusFunctions[fnId](this);
  this.respond("1" + status);
}

// Get all
bcInteract.statusFunctions[0x00] = function (bci) {
  /*
   * Get status of everything
   */
  var dirtyList = [];
  for (iii in bci.entities) {
    if (bci.entities[iii].dirty)
      dirtyList.push(bci.entities[iii]);
  }
  var bc = "";
  for (iii in dirtyList) {
    bc += "[" + dirtyList[iii].sel + "]";
  }
  return bc + "[" + dirtyList.length.toString(16) + "][00]S";
};

// Get an element's status
bcInteract.statusFunctions[0x01] = function (bci) {
  /*
   * element : 1
   */
  var element = bci.entities[bci.stack[bci.stackPtr + 1]];
  var bc = "";
  if (element.type == "cursor") {
    // Return the position
    var pos = parseInt(element.pos * 65335).toString(16);
    bc = "[" + pos + "][" + element.sel.toString(16) + "]";
  }
  element.dirty = false;
  bc += "[01]S"; // Attach status exec vector thingy
  return bc;
};

/* CANVAS HELPERS */

bcInteract.toColour = function (col) {
  return "#" + "0".repeat(6 - col.length) + col;
};

bcInteract.getPixelValues = function (x, y) {
  var cWidth = this.canvas.width;
  var cHeight = this.canvas.height;
  return {
    x : x * (cWidth / 65536),
    y : y * (cHeight / 65536)
  };
};

/* HELPERS */

bcInteract.unpackXY = function (val) {
  var y = val & 0xffff;
  var x = (val - y) >>> 16;
  return [x, y];
};

bcInteract.packedPolyPathHelper = function (ctx, close, start, length) {
  this.ctx.beginPath();
  var xy = this.unpackXY(stack[start]);
  var xyo = this.getPixelValues(xy[0], xy[1]);
  this.ctx.moveTo(xyo.x, xyo.y);
  var iii;
  for (iii = 1; iii < length; iii++) {
    xy = this.unpackXY(stack[iii + start]);
    xyo = this.getPixelValues(xy[0], xy[1]);
    this.ctx.lineTo(xyo.x, xyo.y);
  }
  if (close > 0)
    this.ctx.closePath();
};

bcInteract.executeStackFunction = function () {
  // The stackPtr is the index of the
  // selected execution vector
  var functionId = this.stack[this.stackPtr];
  this.stackFunctions[functionId](this);
};

/* STACK FUNCTIONS */

// Cursor
bcInteract.stackFunctions[0x00] = function (bci) {
  /* stackPtr offsets:
   * selector  : 1
   * colour    : 2
   * direction : 3
   * width     : 4
   * top-left  : 5
   * bot-right : 6
  */
  var sp = bci.stackPtr;
  var sel = bci.stack[sp + 1];
  if (bci.entities[sel] == undefined)
    bci.entities[sel] = {};
  var ent = bci.entities[sel];
  ent.sel = sel;
  ent.colour = bci.toColour(bci.stack[sp + 2].toString(16));
  ent.direction = bci.stack[sp + 3];
  ent.width = bci.stack[sp + 4];
  var tlPacked = bci.stack[sp + 5];
  ent.tl = {};
  ent.tl.y = tlPacked & 0xffff;
  ent.tl.x = (tlPacked - ent.tl.y) >>> 16;
  var brPacked = bci.stack[sp + 6];
  ent.br = {};
  ent.br.y = brPacked & 0xffff;
  ent.br.x = (brPacked - ent.br.y) >>> 16;
  ent.pos = 0.5;
  ent.type = "cursor";
  ent.dirty = true;
};

// hoverSquare
bcInteract.stackFunctions[0x01] = function (bci) {
  /* stackPtr offsets:
   * selector     : 1
   * colour       : 2
   * alpha        : 3
   * top left     : 4
   * bottom right : 5
  */
  var sp = bci.stackPtr;
  var sel = bci.stack[sp + 1];
  if (bci.entities[sel] == undefined)
    bci.entities[sel] = {};
  var ent = bci.entities[sel];
  ent.sel = sel;
  ent.colour = toColour(bci.stack[sp + 2].toString(16));
  var tlPacked = bci.stack[sp + 4];
  ent.tl = {};
  ent.tl.y = tlPacked & 0xffff;
  ent.tl.x = (tlPacked - ent.tl.y) >>> 16;
  var brPacked = bci.stack[sp + 5];
  ent.br = {};
  ent.br.y = brPacked & 0xffff;
  ent.br.x = (brPacked - ent.br.y) >>> 16;
  ent.type = "hoverSquare";
  ent.dirty = true;
};

/* RENDERING */

bcInteract.render = function () {
  // Clear
  this.ctx.clearRect(0,0,this.canvas.width, this.canvas.height);
  // Draw stuff
  this.renderCursors();
};

bcInteract.renderCursors = function () {
  var iii, ent;
  var pt1 = {};
  var pt2 = {};
  var pos;
  for (iii in this.entities) {
    ent = this.entities[iii];
    if (ent.type == "cursor") {
      this.ctx.lineWidth = ent.width;
      this.ctx.strokeStyle = ent.colour;
      if (ent.direction == 0) {
        // Vertical
        pos = ((ent.br.x - ent.tl.x) * ent.pos) + ent.tl.x;
        pt1 = this.getPixelValues(pos, ent.tl.y);
        pt2 = this.getPixelValues(pos, ent.br.y);
      }
      else if (ent.direction == 1) {
        // Horizontal
        pos = ((ent.br.y - ent.tl.y) * ent.pos) + ent.tl.y;
        pt1 = this.getPixelValues(ent.tl.x, pos);
        pt2 = this.getPixelValues(ent.br.x, pos);
      }
      this.ctx.beginPath();
      this.ctx.moveTo(pt1.x, pt1.y);
      this.ctx.lineTo(pt2.x, pt2.y);
      this.ctx.closePath();
      this.ctx.stroke();
    }
  }
};

/* MOUSE INTERACTION */

bcInteract.getCollisionList = function (x, y) {
  var colList = [];
  var ent;
  var epsilon = 2;
  var newEpsilon;
  var hLock = false;
  var vLock = false;
  var pt1 = {};
  var pt2 = {};
  for (iii in this.entities) {
    ent = this.entities[iii];
    if (ent.type == "cursor") {
      var relativePos;
      var success = false;
      newEpsilon = epsilon + (ent.width / 2);
      if (ent.direction == 0 && vLock != true) {
        relativePos = ((ent.br.x - ent.tl.x) * ent.pos) + ent.tl.x;
        pt1 = this.getPixelValues(relativePos, ent.tl.y);
        pt2 = this.getPixelValues(relativePos, ent.br.y);
        pt1.x -= newEpsilon;
        pt2.x += newEpsilon;
        success = true;
      }
      else if (ent.direction == 1 && hLock != true) {
        relativePos = ((ent.br.y - ent.tl.y) * ent.pos) + ent.tl.y;
        pt1 = this.getPixelValues(ent.tl.x, relativePos);
        pt2 = this.getPixelValues(ent.br.x, relativePos);
        pt1.y -= newEpsilon;
        pt2.y += newEpsilon;
        success = true;
      }
      if (success && x >= pt1.x && x <= pt2.x && y >= pt1.y && y <= pt2.y) {
        colList.push(ent);
        if (ent.direction == 0)
          vLock = true;
        else if (ent.direction == 1)
          hLock = true;
      }
    }
  }
  return colList;
};

bcInteract.hoverElements = function (ents) {
  var docStyle = document.body.style;
  var pointerStyles = {
    0     : "ew-resize",
    1     : "ns-resize",
    both  : "crosshair",
    none  : "auto"
  };
  if (ents.length == 1) {
    // It will almost always be 0 or 1
    docStyle.cursor = pointerStyles[ents[0].direction];
  }
  else if (ents.length == 2) {
    // And they're both cursors
    docStyle.cursor = pointerStyles.both;
  }
  else {
    docStyle.cursor = pointerStyles.none;
  }
};

bcInteract.moveCursor = function (cursor, x, y) {
  var tl = this.getPixelValues(cursor.tl.x, cursor.tl.y);
  var br = this.getPixelValues(cursor.br.x, cursor.br.y);
  if (cursor.direction == 0) {
    cursor.pos = (x - tl.x) / (br.x - tl.x);
  }
  else if (cursor.direction == 1) {
    cursor.pos = (y - tl.y) / (br.y - tl.y);
  }
  if (cursor.pos < 0)
    cursor.pos = 0;
  else if (cursor.pos > 1)
    cursor.pos = 1;
  cursor.dirty = true;
};

bcInteract.entsHovered = [];
bcInteract.entsHeld = [];

bcInteract.connectCanvasEvents = function () {
  var bci = this; // 'this' won't work in anon-fn scope
  this.canvas.addEventListener('mousemove', function (evt) {
    var rect = bci.canvas.getBoundingClientRect();
    var mouse = {
      x : evt.clientX - rect.left,
      y : evt.clientY - rect.top
    };
    if (bci.entsHeld.length) {
      for (iii in bci.entsHeld) {
        var ent = bci.entsHeld[iii];
        if (ent.type = "cursor")
          bci.moveCursor(ent, mouse.x, mouse.y);
      }
    }
    bci.entsHovered = bci.getCollisionList(mouse.x, mouse.y)
    bci.hoverElements(bci.entsHovered);
  });

  this.canvas.addEventListener('mousedown', function (evt) {
    bci.entsHeld = bci.entsHovered;
  });

  this.canvas.addEventListener('mouseup', function (evt) {
    bci.entsHeld = [];
  });
};
