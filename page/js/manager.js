var bcManager = {};

bcManager.bcd = bcDisplay;
bcManager.bci = bcInteract;
bcManager.bcDoc = bcDocument;

/* CAMVAS */

bcManager.assignCanvas = function (selector, canvas) {
  switch (selector) {
    case "display":
      bcDisplay.setCanvas(canvas);
      break;
    case "input":
      bcInteract.setCanvas(canvas);
      break;
    default:
      return "Bad selector";
  }
};

/* BYTECODE PASSING */

bcManager.routeCode = function (bc) {
  var vmId = parseInt(bc[0]);
  // Offset by 1 because of that pesky vm selector
  switch (vmId) {
    case 0:
      bcDisplay.decode(bc, 1);
      break;
    case 1:
      bcInteract.decode(bc, 1);
      break;
    case 2:
      bcDocument.decode(bc, 1);
      break;
  }
};

bcManager.setResponseCallbacks = function () {
  var socketSend = function (msg) { bcManager.comms.socket.send(msg); }; // WHY
  // Interaction
  bcInteract.respond = function (msg) {
    socketSend(msg);
  };
  bcDocument.respond = function (msg) {
    socketSend(msg);
  };
};

/* WEBSOCKETRY */

bcManager.comms = {};
bcManager.comms.uri = "";
bcManager.comms.socket = null;

bcManager.comms.setUri = function (addr, port) {
  this.uri = "ws://" + addr + ":" + port + "/ws";
};

bcManager.comms.connect = function () {
  if (this.socket != null)
    this.socket.close();
  this.socket = new WebSocket(this.uri);
  this.socket.onopen = this.onOpen;
  this.socket.onclose = this.onClose;
  this.socket.onerror = this.onError;
  this.socket.onmessage = this.onMessage;
};

bcManager.comms.onOpen = function (evt) {
  bcManager.setResponseCallbacks();
};

bcManager.comms.onClose = function (evt) {};
bcManager.comms.onError = function (evt) {};

bcManager.comms.onMessage = function (evt) {
  bcManager.routeCode(evt.data);
};

// Initialise
bcManager.getDocEle = function (id) { return document.getElementById(id); };

//bcManager.assignCanvas("input", bcManager.getDocEle("input_canvas"));
bcManager.assignCanvas("display", bcManager.getDocEle("main_canvas"));

bcManager.bcDoc.registerButton(bcManager.getDocEle("toggle_trigger"), 0);
bcManager.bcDoc.registerButton(bcManager.getDocEle("change_layout"), 1);
bcManager.bcDoc.registerSlider(bcManager.getDocEle("timebase"), 2);
bcManager.bcDoc.registerButton(bcManager.getDocEle("reset"), 3);
bcManager.bcDoc.registerSlider(bcManager.getDocEle("trigger_level"), 4);
bcManager.bcDoc.registerButton(bcManager.getDocEle("range_up"), 5);
bcManager.bcDoc.registerButton(bcManager.getDocEle("range_down"), 6);

// Copy page url to host address
var hostAddr = window.location.href;
hostAddr = hostAddr.replace("http://","").replace(":8080/", "");
document.getElementById("host").value = hostAddr;

// Connection input
document.getElementById("connect").addEventListener("click", function (evt) {
  var host = document.getElementById("host").value;
  var port = document.getElementById("port").value;
  bcManager.comms.setUri(host, port);
  bcManager.comms.connect();
});
