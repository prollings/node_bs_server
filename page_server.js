"use strict"

let connect = require("connect");
let serveStatic = require("serve-static");

let app;

function start() {
  app = connect().use(serveStatic(__dirname + "/page/"));
  app.listen(8080);
}

exports.start = start;
