"use strict"

let ws = require("ws");

let port = 8001;
let logging = false;

let server;

let openCallback = function (conn) {
  console.log("Client (", conn.headers.host, ") connected");
};

let receiveCallback = function (str) {
  console.log(str);
};

let closeCallback = function (code, reason) {
  console.log("Client disconnected, code: ", code, " reason: ", reason);
};

function setOpenCallback(callback) {
  openCallback = callback;
}

function setRecieveCallback(callback) {
  receiveCallback = callback;
}

function setCloseCallback(callback) {
  closeCallback = callback;
}

function setLogging(enable) {
  logging = enable;
}

function getWebsocketKey (conn) {
  return conn.upgradeReq.headers["sec-websocket-key"];
}

function start() {
  server = new ws.Server({ port : port });
  server.on('connection', function (conn) {
    console.log("Client connected.");
    conn.on('message', receiveCallback || function (str) {
      console.log(str);
    });
    conn.on('close', closeCallback || function (code, message) {
      console.log("Client disconnected, code:", code, "message:", message);
    });
    conn.on('error', function (error) { console.log(error); });
  });

  server.broadcast = function (data) {
    server.clients.forEach(function (client) {
      try {
        client.send(data);
      }
      catch (e) {
        console.log(e);
        client.close();
      }
    });
  };
}

function sendText(str) {
  if (logging)
    console.log(str);
  server.broadcast(str);
}

exports.setOpenCallback = setOpenCallback;
exports.setRecieveCallback = setRecieveCallback;
exports.setCloseCallback = setCloseCallback;
exports.start = start;
exports.sendText = sendText;
